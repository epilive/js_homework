function NOD (x, y) {
      while (x && y) {
        x > y ? x %= y : y %= x;
      }
      x += y;
    return x;
  }
  
  console.log(NOD( 28, 16 ));