function generate_array(number_of_elements, min_random_num, max_random_num){
    let arr = new Array();
    if(number_of_elements > (max_random_num - min_random_num + 1)){
        return arr;
    }

    while(arr.length != number_of_elements){
        let rand = getRandomInt(min_random_num, max_random_num);
        if(!arr.includes(rand)){
            arr.push(rand);
        } else {
            continue;
        }
    }

    return arr;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

console.log(generate_array(50, -25, 25));


