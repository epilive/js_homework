let array = [0, 1, 2, 3, 4, 5];

for(let i = 0; i < array.length; i++){
    console.log(array[i]);
}

for(variable in array){
    console.log(variable);
}

for(variable of array){
    console.log(variable);
}
