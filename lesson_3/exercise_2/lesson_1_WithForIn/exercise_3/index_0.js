let random_number = getRandomInt(1,100);

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function toDigits(num) {
    const digits = [];

    for (let numCopy = num; numCopy >= 1; numCopy/=10) {
        const digit = Math.floor(numCopy) % 10;
        digits.unshift(digit);
    }
    
    return digits;
}

function sumDigits(digits = []){
    var sum = 0;

    for(var i = 0; i <digits.length; i++){
        if(digits[i]%2 == 0){
            sum += digits[i];
        }
    }

    return sum;
}

console.log("Number: " + random_number);
console.log("Digits: " + toDigits(random_number));
console.log("Sum: " + sumDigits(toDigits(random_number)));
