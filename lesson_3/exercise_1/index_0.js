function generate_array(number_of_elements, min_random_num, max_random_num){
    let arr = new Array();
    if(number_of_elements > (max_random_num - min_random_num + 1)){
        return arr;
    }

    while(arr.length != number_of_elements){
        let rand = getRandomInt(min_random_num, max_random_num);
        if(!arr.includes(rand)){
            arr.push(rand);
        } else {
            continue;
        }
    }

    console.log(arr);
    return arr;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function array_elements_percent(arr){
    let positive = 0;
    let negative = 0;
    let zero = 0;

    for(let i = 0; i < arr.length; i++){
        if(arr[i] > 0){
            positive++;
        } else if(arr[i] < 0){
            negative++;
        } else {
            zero++;
        }
    }

    positive = Math.floor(positive / arr.length * 100);
    negative = Math.floor(negative / arr.length * 100);
    zero = Math.floor(zero / arr.length * 100);

    console.log("Array length: " + arr.length + " = 100%");
    console.log("Positive numbers percentage: " + positive + "%");
    console.log("Negative numbers percentage: " + negative + "%");
    console.log("Zero's percentage: " + zero + "%");
}


let array = generate_array(51, -25, 25);

array_elements_percent(array);