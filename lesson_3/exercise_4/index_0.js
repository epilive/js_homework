// итак, пусть будет задан некий абстрактный массив 
// с произвольной глубиной вложенности
// при этом элементами массива могут быть
// строки и вложенные массивы
const arr = [
  "string 1",
  "string 2",
  [
    "string 3.1",
    "string 3.2",
    [
      "string 3.3.1",
      [
        "string 3.3.2.1",
        "string 3.3.2.2"
      ],
      "string 3.3.3",
    ]
  ],
  "string 4",
  [
    "string 5.1",
    [
      "string 5.2.1",
      [
        "string 5.2.2.1",
        "string 5.2.2.2"
      ],
      "string 5.2.3"
    ]
  ]
];


function parseArray(data) {
  let retstr = '';
  if(typeof data === "string"){

    retstr += '<li>'+data+'</li>';
  }else if(Array.isArray(data) ){
    retstr += '<ul>';

    data.forEach(value=>{
      retstr += parseArray(value);
    });

    retstr += `</ul>`;
  }
  return retstr;
}


let wrap = document.querySelector('.wrap');
wrap.innerHTML = parseArray(arr);