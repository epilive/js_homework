var random_digital_storage_value = getRandomInt(0,3);
var digital_storage;
var random_number = getRandomInt(1,10);
var bytes;


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


switch(random_digital_storage_value)
{
    case 0: digital_storage = "Bytes"; bytes = random_number;
    break;
    case 1: digital_storage = "KB"; bytes = random_number * 1024;  
    break;
    case 2: digital_storage = "MB"; bytes = random_number * 1024 * 1024; 
    break;
    case 3: digital_storage = "GB"; bytes = random_number * 1024 * 1024 * 1024; 
    break;
}

console.log(random_number + " " + digital_storage + " = " + bytes + " Bytes");

