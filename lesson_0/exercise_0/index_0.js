var numeral = getRandomInt(0,9);

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

console.log("Numeral: " + numeral);


if (numeral == 0) 
{
    alert("Zero"); 
} 
else if(numeral == 1)
{
    alert("One"); 
}
else if(numeral == 2)
{
    alert("Two"); 
}
else if(numeral == 3)
{
    alert("Three"); 
}
else if(numeral == 4)
{
    alert("Four"); 
}
else if(numeral == 5)
{
    alert("Five"); 
}
else if(numeral == 6)
{
    alert("Six"); 
}
else if(numeral == 7)
{
    alert("Seven"); 
}
else if(numeral == 8)
{
    alert("Eight"); 
}
else
{
    alert("Nine");
}