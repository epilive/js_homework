let matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];

console.log(matrix);


let t_matrix;


function transpose_matrix(mtrx){
    this.mtrx = mtrx;
    this.t_mtrx = [];
    this.size = mtrx[0].length;

    for(let i = 1; i < mtrx.length; i ++){
        if(mtrx[i].length != this.size){
            console.log(" NOT A MATRIX ");
            return "not a matrix";
        }
    }

    for(let i = 0; i < this.mtrx.length; i++){
        for(let j = 0; j < this.mtrx[i].length; j ++){

            if (typeof this.t_mtrx[j] == 'undefined'){
                this.t_mtrx[j] = new Array(this.mtrx.length)
            }

            this.t_mtrx[j][i] = this.mtrx[i][j];
        }
    }

    return this.t_mtrx;
}

t_matrix = transpose_matrix(matrix);

console.log(t_matrix);