let array = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8] 
]


function wich_row_max_sum(arr){
    let row_id = 0;
    let max_sum = 0;
    
    let sum_array_row = new Array(arr[0].length);
    sum_array_row.fill(0);

    for(let i = 0; i < arr.length; i ++){
        for(let j = 0; j < arr[i].length; j++){
            sum_array_row[i] += arr[i][j];
        }   
    }

    for(let i = 0; i < sum_array_row.length; i++){
        if(max_sum < sum_array_row[i]){
            max_sum = sum_array_row[i];
        }
    }

    row_id = sum_array_row.indexOf(max_sum) + 1;

    return row_id;
}


function wich_column_max_sum(arr){
    let column_id = 0;
    let max_sum = 0;
    
    let sum_array_column = new Array(arr.length);
    sum_array_column.fill(0);

    for(let i = 0; i < arr.length; i ++){
        for(let j = 0; j < arr[i].length; j++){
            sum_array_column[i] += arr[j][i];
        }   
    }

    for(let i = 0; i < sum_array_column.length; i++){
        if(max_sum < sum_array_column[i]){
            max_sum = sum_array_column[i];
        }
    }

    row_id = sum_array_column.indexOf(max_sum) + 1;

    return row_id;
}

console.log(array);

console.log("The row that has max sum is row №" + wich_row_max_sum(array));

console.log("The column that has max sum is column №" + wich_column_max_sum(array));


