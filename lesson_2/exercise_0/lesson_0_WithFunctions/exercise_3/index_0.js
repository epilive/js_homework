var credit_uah = 30000;
var percent = 25;
var years = 3;

calculate_credit(credit_uah, percent, years);
function calculate_credit(uah, perc, year){
    var one_year_percent;
    var all_time_percent;
    var all_money;

    one_year_percent = credit_uah / 100 * percent / 365 * 30 * 12;
    all_time_percent = one_year_percent * years;
    all_money = credit_uah + all_time_percent;

    console.log("Variables: ");
    console.log("Credit: " + credit_uah);
    console.log("Percent: " + percent);
    console.log("Years: " + years);
    console.log("");
    console.log("Result: ");
    console.log("First year percent: " + one_year_percent);
    console.log("All time percent: " + all_time_percent);
    console.log("All money spent: " + all_money);
}
