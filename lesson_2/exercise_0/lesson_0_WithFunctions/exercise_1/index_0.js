var random_number = getRandomInt(-2,2);

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

console.log("Number: " + random_number);
check_number();

function check_number(){
    if(random_number == 0)
    {
        alert("Number is equal to Zero");
    }
    else if(random_number > 0)
    {
        alert("Number is greater than Zero");
    }
    else
    {
        alert("Number is less than Zero");
    }
}
