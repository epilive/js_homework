var random_digital_storage_value = getRandomInt(0,3);
var random_number = getRandomInt(1,10);


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

generate_random_storage_and_calculate(random_digital_storage_value, random_number);

function generate_random_storage_and_calculate(random_storage, number){
    let bytes;
    let digital_storage;

    switch(random_storage)
    {
        case 0: digital_storage = "Bytes"; bytes = number;
        break;
        case 1: digital_storage = "KB"; bytes = number * 1024;  
        break;
        case 2: digital_storage = "MB"; bytes = number * 1024 * 1024; 
        break;
        case 3: digital_storage = "GB"; bytes = number * 1024 * 1024 * 1024; 
        break;
    }

    console.log(number + " " + digital_storage + " = " + bytes + " Bytes");
}

