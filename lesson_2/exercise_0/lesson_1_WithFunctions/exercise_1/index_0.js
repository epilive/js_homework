var random_number = getRandomInt(1,10);


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function calculateFactorial(number){
    var result = number;
    for(var i = number - 1; i > 0; i--){
        result *= i;
    }
    
    return result;
}

console.log("Number: " + random_number);
console.log("Factorial: " + calculateFactorial(random_number));