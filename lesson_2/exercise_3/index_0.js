let first_matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];


let second_matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];

let result_matrix;


function multiply_matrices(a, b) {
    var aNumRows = a.length, aNumCols = a[0].length,
        bNumRows = b.length, bNumCols = b[0].length,
        m = new Array(aNumRows);  
    for (var r = 0; r < aNumRows; ++r) {
      m[r] = new Array(bNumCols); 
      for (var c = 0; c < bNumCols; ++c) {
        m[r][c] = 0;             
        for (var i = 0; i < aNumCols; ++i) {
          m[r][c] += a[r][i] * b[i][c];
        }
      }
    }
    return m;
  }

result_matrix = multiply_matrices(first_matrix, second_matrix);

console.log("First matrix: ");
console.log(first_matrix);
console.log("Second matrix: ");
console.log(second_matrix);
console.log("Result matrix: ");
console.log(result_matrix);