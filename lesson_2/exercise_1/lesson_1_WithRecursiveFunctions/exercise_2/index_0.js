let random_number = getRandomInt(1,100);
let output = [];
let i = 1


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


get_number_dividers();


function get_number_dividers(){
    if (random_number % i == 0){
         output.push(random_number / i);  
    }

    i++;

    if(i <= random_number){
        get_number_dividers();
    }
}


console.log("Random Number: " + random_number);

console.log("Dividers: " + output);